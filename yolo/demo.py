import cv2
import numpy as np
import argparse

# construct the argument parse and parse the arguments
ap = argparse.ArgumentParser()
ap.add_argument("-i", "--image", required=True, help="path to input image")
ap.add_argument("-p", "--prototxt", required=True, help="path to Caffe 'deploy' prototxt file")
ap.add_argument("-m", "--model", required=True, help="model weights")
ap.add_argument("-cfg", "--config", required=True, help="model configuration")
ap.add_argument("-c", "--confidence", type=float, default=0.2, help="minimum probability to filter weak detections")
args = vars(ap.parse_args())

# load our serialized model from disk
print("[INFO] loading config...")


# load our serialized model from disk
print("[INFO] loading model...")
net = cv2.dnn.readNetFromDarknet(args["model"],args["config"])

if net.empty():
    print("[ERROR] Can't load network using the following files")
    print("[ERROR] cfg-file: " + args["config"])
    print("[ERROR] weights-file: " + args["model"])
    exit(-1)
